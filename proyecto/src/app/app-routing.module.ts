import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventarioComponent } from './components/inventario/inventario.component'
import { CajaComponent } from './components/caja/caja.component'
import { UsuariosComponent } from './components/usuarios/usuarios.component'
import { VentasComponent } from './components/ventas/ventas.component'
import { ErrorComponent } from './components/error/error.component'
import { BienvenidoComponent } from './components/bienvenido/bienvenido.component'

const routes: Routes = [
  { path: 'inventario', component: InventarioComponent},
  { path:'caja', component: CajaComponent},
  { path: 'usuarios', component: UsuariosComponent},
  { path: 'ventas', component: VentasComponent},
  { path: '**', component: ErrorComponent},
  { path: '*', component: BienvenidoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
