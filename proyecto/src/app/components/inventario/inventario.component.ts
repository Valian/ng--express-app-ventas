import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../services/producto.service'


@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.css']
})
export class InventarioComponent implements OnInit {
  public productos = null;
  public editar: number = 0;
  public eliminar:number = 0;
  public ver: number = 0;
  public prod;
  public searchText:any;
  constructor(protected productoService: ProductoService) { }

  ngOnInit() {
    this.getAll();
  }
  getAll() {
    this.productoService.getProductos().subscribe(data=>(this.productos = data));
  }
  getOne(id:number){
    this.productoService.getProducto(id).subscribe(data=>(this.prod = data))
  }
  delete(id:number){
    this.productoService.delete(id);
    this.getAll()
  }
}
