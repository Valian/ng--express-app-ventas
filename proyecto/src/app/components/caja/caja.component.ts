import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../services/producto.service'
import { VentasService } from '../../services/ventas.service'
import { Venta } from '../../models/Venta'

@Component({
  selector: 'app-caja',
  templateUrl: './caja.component.html',
  styleUrls: ['./caja.component.css']
})
export class CajaComponent implements OnInit {
  public total: number = 0;
  public producto: any;
  public idProducto: number;
  public boletas: any=[];
  public productos: any=[];
  public vendedor:number = 1;
  public venta: Venta;

  constructor(protected productoServices : ProductoService, protected ventaService : VentasService) { }
  ngOnInit() {
  }
  lanzar(id: number) {
    this.buscarProducto(id);
  }
  buscarProducto(id:number){
    this.productoServices.getProducto(id).subscribe(
      data => {
        if(data){
          this.producto = data;
          this.boletas.push(data);
          let precio = data['precio'];
          this.total += precio
          this.idProducto = null;
          this.productos.push(data['codigo']);
        }
      });
  }
  nuevaVenta(){
    this.ventaService.postVenta(this.vendedor)
    let id = this.ventaService.getUltimo();
    console.log(id)
    for(let producto of this.productos){
      this.ventaService.postDetalleVenta(
        {
          vendedor: this.vendedor,
          venta : id,
          producto : producto
       }
      )
    }
      
  
    this.boletas = [];
    this.productos = [];
    this.total = 0;
    this.producto=null;
  }
}
