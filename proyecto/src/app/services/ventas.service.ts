import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class VentasService {
  private url = 'http://localhost:3000';
  private urlVenta = 'http://localhost:3000/nuevo';

  constructor(protected httpClient: HttpClient) { }
  getVentas() {
    return this.httpClient.get(this.url);
  }
  postVenta(vendedor: number) {
    return this.httpClient.post(this.urlVenta, {
      Vendedor: vendedor
    }).subscribe((data) => {
      console.log("postVenta success");
    });
  }
  postDetalleVenta(venta) {
    this.httpClient.post(this.urlVenta, {
      Vendedor: venta.vendedor,
      idVenta: venta.venta,
      idProducto: venta.producto
    }).subscribe((data) => {
      console.log("postDetalleVenta success");
    });
  }
  getUltimo() {
    return this.httpClient.get('http://localhost:3000/ultimo').subscribe((data) => {
      console.log("ultimo success");
    });
  }
}
