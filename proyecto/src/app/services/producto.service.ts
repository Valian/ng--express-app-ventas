import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Producto } from '../models/Producto'


@Injectable({
  providedIn: 'root'
})
export class ProductoService implements OnInit{

  private url = 'http://localhost:3000';
  constructor(private httpClient: HttpClient) { }

  ngOnInit(){

  }
  getProductos(){
    return this.httpClient.get<Producto>(this.url)
  }
  getProducto(id:number){
    return this.httpClient.get(this.url+'/'+id)
  }
  delete(id:number){
    this.httpClient.delete(this.url+'/'+id).subscribe((data)=>{
      console.log("success");
    });
  }

}
