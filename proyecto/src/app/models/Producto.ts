export interface Producto {
    idProductos : number;
    nombre      : string;
    codigo      : number;
    precio      : number;
    stock       : number
}