-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-03-2019 a las 02:36:11
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mydb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleVenta`
--

CREATE TABLE `detalleVenta` (
  `iddetalleVenta` int(11) NOT NULL,
  `vendedor` int(11) DEFAULT NULL,
  `venta` int(11) DEFAULT NULL,
  `producto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Productos`
--

CREATE TABLE `Productos` (
  `idProductos` int(11) NOT NULL,
  `codigo` int(12) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `precio` int(10) DEFAULT NULL,
  `stock` int(45) DEFAULT NULL,
  `comentario` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `Productos`
--

INSERT INTO `Productos` (`idProductos`, `codigo`, `nombre`, `precio`, `stock`, `comentario`) VALUES
(1, 26212497, 'Producto numero : 0', 9662, 130, 'Este es el producto '),
(2, 56472526, 'Producto numero : 1', 73604, 531, 'Este es el producto '),
(3, 27117393, 'Producto numero : 2', 13463, 194, 'Este es el producto '),
(4, 75516208, 'Producto numero : 3', 72108, 987, 'Este es el producto '),
(5, 80566960, 'Producto numero : 4', 70566, 705, 'Este es el producto '),
(6, 58732407, 'Producto numero : 5', 1306, 871, 'Este es el producto '),
(7, 81207021, 'Producto numero : 6', 81067, 817, 'Este es el producto '),
(8, 65047360, 'Producto numero : 7', 3523, 628, 'Este es el producto '),
(9, 87834778, 'Producto numero : 8', 87659, 143, 'Este es el producto '),
(10, 78678294, 'Producto numero : 9', 53727, 745, 'Este es el producto '),
(11, 42912401, 'Producto numero : 10', 40672, 869, 'Este es el producto '),
(12, 91623064, 'Producto numero : 11', 18961, 473, 'Este es el producto '),
(13, 94382098, 'Producto numero : 12', 12145, 731, 'Este es el producto '),
(14, 25099314, 'Producto numero : 13', 9853, 658, 'Este es el producto '),
(15, 12950541, 'Producto numero : 14', 56006, 129, 'Este es el producto '),
(16, 48900083, 'Producto numero : 15', 4934, 282, 'Este es el producto '),
(17, 39522319, 'Producto numero : 16', 20319, 44, 'Este es el producto '),
(18, 94571165, 'Producto numero : 17', 88248, 942, 'Este es el producto '),
(19, 89123500, 'Producto numero : 18', 96673, 528, 'Este es el producto '),
(20, 37499366, 'Producto numero : 19', 50348, 239, 'Este es el producto '),
(21, 81353387, 'Producto numero : 20', 26336, 466, 'Este es el producto '),
(22, 27987416, 'Producto numero : 21', 95571, 323, 'Este es el producto '),
(23, 88122785, 'Producto numero : 22', 84794, 699, 'Este es el producto '),
(24, 31268374, 'Producto numero : 23', 14014, 468, 'Este es el producto '),
(25, 44697855, 'Producto numero : 24', 20044, 957, 'Este es el producto '),
(26, 77449607, 'Producto numero : 25', 64434, 826, 'Este es el producto '),
(27, 77167410, 'Producto numero : 26', 10184, 391, 'Este es el producto '),
(28, 32769554, 'Producto numero : 27', 29167, 66, 'Este es el producto '),
(29, 55458001, 'Producto numero : 28', 27673, 665, 'Este es el producto '),
(30, 16679865, 'Producto numero : 29', 33263, 869, 'Este es el producto '),
(31, 48912650, 'Producto numero : 30', 64657, 404, 'Este es el producto '),
(32, 14886604, 'Producto numero : 31', 35500, 959, 'Este es el producto '),
(33, 15399599, 'Producto numero : 32', 56495, 365, 'Este es el producto '),
(34, 86395389, 'Producto numero : 33', 59954, 941, 'Este es el producto '),
(35, 91285371, 'Producto numero : 34', 19296, 403, 'Este es el producto '),
(36, 90556167, 'Producto numero : 35', 19243, 952, 'Este es el producto '),
(37, 53597357, 'Producto numero : 36', 40314, 34, 'Este es el producto '),
(38, 70756478, 'Producto numero : 37', 39934, 715, 'Este es el producto '),
(39, 50159632, 'Producto numero : 38', 88978, 331, 'Este es el producto '),
(40, 45078594, 'Producto numero : 39', 77292, 497, 'Este es el producto '),
(41, 36758681, 'Producto numero : 40', 14155, 201, 'Este es el producto '),
(42, 16803110, 'Producto numero : 41', 86959, 406, 'Este es el producto '),
(43, 63071453, 'Producto numero : 42', 92282, 582, 'Este es el producto '),
(44, 13001446, 'Producto numero : 43', 40611, 905, 'Este es el producto '),
(45, 77924136, 'Producto numero : 44', 3238, 233, 'Este es el producto '),
(46, 41241988, 'Producto numero : 45', 99690, 656, 'Este es el producto '),
(47, 16370193, 'Producto numero : 46', 74982, 448, 'Este es el producto '),
(48, 55069089, 'Producto numero : 47', 19190, 131, 'Este es el producto '),
(49, 44548032, 'Producto numero : 48', 40178, 706, 'Este es el producto '),
(50, 39508066, 'Producto numero : 49', 78473, 908, 'Este es el producto '),
(51, 43427353, 'Producto numero : 50', 29121, 528, 'Este es el producto '),
(52, 61636147, 'Producto numero : 51', 44421, 68, 'Este es el producto '),
(53, 66169558, 'Producto numero : 52', 16756, 511, 'Este es el producto '),
(54, 78431474, 'Producto numero : 53', 94585, 838, 'Este es el producto '),
(55, 76597540, 'Producto numero : 54', 13617, 836, 'Este es el producto '),
(56, 95972942, 'Producto numero : 55', 79183, 680, 'Este es el producto '),
(57, 93702899, 'Producto numero : 56', 10051, 497, 'Este es el producto '),
(58, 18944881, 'Producto numero : 57', 67647, 285, 'Este es el producto '),
(59, 34504188, 'Producto numero : 58', 82512, 725, 'Este es el producto '),
(60, 81737890, 'Producto numero : 59', 34359, 247, 'Este es el producto '),
(61, 26079582, 'Producto numero : 60', 51447, 728, 'Este es el producto '),
(62, 66376516, 'Producto numero : 61', 93380, 66, 'Este es el producto '),
(63, 21319303, 'Producto numero : 62', 17563, 171, 'Este es el producto '),
(64, 39924841, 'Producto numero : 63', 32354, 307, 'Este es el producto '),
(65, 71884153, 'Producto numero : 64', 21419, 119, 'Este es el producto '),
(66, 62061182, 'Producto numero : 65', 15889, 227, 'Este es el producto '),
(67, 92190209, 'Producto numero : 66', 74733, 272, 'Este es el producto '),
(68, 69163164, 'Producto numero : 67', 40807, 843, 'Este es el producto '),
(69, 16988537, 'Producto numero : 68', 22520, 245, 'Este es el producto '),
(70, 25130620, 'Producto numero : 69', 58198, 505, 'Este es el producto '),
(71, 54089845, 'Producto numero : 70', 44183, 563, 'Este es el producto '),
(72, 56503776, 'Producto numero : 71', 86007, 397, 'Este es el producto '),
(73, 28963495, 'Producto numero : 72', 66226, 418, 'Este es el producto '),
(74, 61275638, 'Producto numero : 73', 61256, 725, 'Este es el producto '),
(75, 37392809, 'Producto numero : 74', 12817, 85, 'Este es el producto '),
(76, 59816451, 'Producto numero : 75', 9390, 359, 'Este es el producto '),
(77, 76654364, 'Producto numero : 76', 67438, 395, 'Este es el producto '),
(78, 59106833, 'Producto numero : 77', 58601, 221, 'Este es el producto '),
(79, 46125599, 'Producto numero : 78', 1919, 585, 'Este es el producto '),
(80, 56962859, 'Producto numero : 79', 10472, 54, 'Este es el producto '),
(81, 81219924, 'Producto numero : 80', 30818, 196, 'Este es el producto '),
(82, 40109818, 'Producto numero : 81', 23808, 569, 'Este es el producto '),
(83, 25550916, 'Producto numero : 82', 80688, 250, 'Este es el producto '),
(84, 65626443, 'Producto numero : 83', 86396, 410, 'Este es el producto '),
(85, 67601919, 'Producto numero : 84', 52701, 280, 'Este es el producto '),
(86, 36509635, 'Producto numero : 85', 45940, 137, 'Este es el producto '),
(87, 19652336, 'Producto numero : 86', 36774, 137, 'Este es el producto '),
(88, 43911198, 'Producto numero : 87', 3012, 522, 'Este es el producto '),
(89, 60621971, 'Producto numero : 88', 60354, 437, 'Este es el producto '),
(90, 54687640, 'Producto numero : 89', 58883, 271, 'Este es el producto '),
(91, 31476289, 'Producto numero : 90', 93153, 623, 'Este es el producto '),
(92, 89859646, 'Producto numero : 91', 30288, 666, 'Este es el producto '),
(93, 36192481, 'Producto numero : 92', 37165, 331, 'Este es el producto '),
(94, 75840570, 'Producto numero : 93', 40713, 447, 'Este es el producto '),
(95, 28119911, 'Producto numero : 94', 86153, 592, 'Este es el producto '),
(96, 50096917, 'Producto numero : 95', 55411, 185, 'Este es el producto '),
(97, 38573134, 'Producto numero : 96', 8986, 384, 'Este es el producto '),
(98, 63276060, 'Producto numero : 97', 51651, 522, 'Este es el producto '),
(99, 21902199, 'Producto numero : 98', 35319, 834, 'Este es el producto '),
(100, 60787125, 'Producto numero : 99', 17111, 327, 'Este es el producto '),
(101, 57819899, 'Producto numero : 100', 22198, 680, 'Este es el producto '),
(102, 93912806, 'Producto numero : 101', 1100, 754, 'Este es el producto '),
(103, 15196335, 'Producto numero : 102', 31727, 378, 'Este es el producto '),
(104, 84049756, 'Producto numero : 103', 83652, 588, 'Este es el producto '),
(105, 93505459, 'Producto numero : 104', 41744, 23, 'Este es el producto '),
(106, 30298290, 'Producto numero : 105', 41269, 940, 'Este es el producto '),
(107, 24651818, 'Producto numero : 106', 42438, 736, 'Este es el producto '),
(108, 44679598, 'Producto numero : 107', 3855, 246, 'Este es el producto '),
(109, 17505250, 'Producto numero : 108', 72069, 446, 'Este es el producto '),
(110, 58976308, 'Producto numero : 109', 18651, 92, 'Este es el producto '),
(111, 51723993, 'Producto numero : 110', 12461, 360, 'Este es el producto '),
(112, 71844697, 'Producto numero : 111', 97544, 699, 'Este es el producto '),
(113, 90270071, 'Producto numero : 112', 77021, 556, 'Este es el producto '),
(114, 65460481, 'Producto numero : 113', 16770, 429, 'Este es el producto '),
(115, 71954042, 'Producto numero : 114', 37071, 894, 'Este es el producto '),
(116, 48785271, 'Producto numero : 115', 1404, 483, 'Este es el producto '),
(117, 33858661, 'Producto numero : 116', 25356, 692, 'Este es el producto '),
(118, 50353674, 'Producto numero : 117', 83033, 840, 'Este es el producto '),
(119, 16186746, 'Producto numero : 118', 38927, 816, 'Este es el producto '),
(120, 68474961, 'Producto numero : 119', 27975, 165, 'Este es el producto '),
(121, 97336334, 'Producto numero : 120', 25736, 888, 'Este es el producto '),
(122, 14564401, 'Producto numero : 121', 29326, 350, 'Este es el producto '),
(123, 22026039, 'Producto numero : 122', 38150, 886, 'Este es el producto '),
(124, 14460695, 'Producto numero : 123', 45260, 83, 'Este es el producto '),
(125, 53221259, 'Producto numero : 124', 43608, 670, 'Este es el producto '),
(126, 29054355, 'Producto numero : 125', 31781, 529, 'Este es el producto '),
(127, 49743596, 'Producto numero : 126', 75797, 402, 'Este es el producto '),
(128, 91615461, 'Producto numero : 127', 35912, 180, 'Este es el producto '),
(129, 43761798, 'Producto numero : 128', 76269, 624, 'Este es el producto '),
(130, 75207030, 'Producto numero : 129', 19982, 976, 'Este es el producto '),
(131, 69280922, 'Producto numero : 130', 23917, 910, 'Este es el producto '),
(132, 35867083, 'Producto numero : 131', 70353, 340, 'Este es el producto '),
(133, 72284062, 'Producto numero : 132', 56184, 926, 'Este es el producto '),
(134, 82523762, 'Producto numero : 133', 90105, 376, 'Este es el producto '),
(135, 71075500, 'Producto numero : 134', 60232, 898, 'Este es el producto '),
(136, 83779328, 'Producto numero : 135', 71831, 485, 'Este es el producto '),
(137, 20918958, 'Producto numero : 136', 59006, 67, 'Este es el producto '),
(138, 52930163, 'Producto numero : 137', 87188, 658, 'Este es el producto '),
(139, 88213062, 'Producto numero : 138', 75855, 893, 'Este es el producto '),
(140, 79822450, 'Producto numero : 139', 62822, 295, 'Este es el producto '),
(141, 92087895, 'Producto numero : 140', 16897, 395, 'Este es el producto '),
(142, 22555794, 'Producto numero : 141', 97232, 552, 'Este es el producto '),
(143, 97895002, 'Producto numero : 142', 12907, 454, 'Este es el producto '),
(144, 35339218, 'Producto numero : 143', 75411, 431, 'Este es el producto '),
(145, 56877103, 'Producto numero : 144', 74102, 97, 'Este es el producto '),
(146, 79987668, 'Producto numero : 145', 87139, 369, 'Este es el producto '),
(147, 92370144, 'Producto numero : 146', 33458, 310, 'Este es el producto '),
(148, 63765706, 'Producto numero : 147', 75148, 126, 'Este es el producto '),
(149, 28918331, 'Producto numero : 148', 5946, 473, 'Este es el producto '),
(150, 31596917, 'Producto numero : 149', 46649, 920, 'Este es el producto '),
(151, 17798395, 'Producto numero : 150', 80637, 40, 'Este es el producto '),
(152, 19574650, 'Producto numero : 151', 38802, 107, 'Este es el producto '),
(153, 19478441, 'Producto numero : 152', 43697, 628, 'Este es el producto '),
(154, 50907009, 'Producto numero : 153', 82755, 107, 'Este es el producto '),
(155, 34803930, 'Producto numero : 154', 44847, 361, 'Este es el producto '),
(156, 15025373, 'Producto numero : 155', 14226, 165, 'Este es el producto '),
(157, 25227865, 'Producto numero : 156', 97533, 238, 'Este es el producto '),
(158, 62085824, 'Producto numero : 157', 57701, 540, 'Este es el producto '),
(159, 19813621, 'Producto numero : 158', 47496, 232, 'Este es el producto '),
(160, 59823753, 'Producto numero : 159', 70343, 843, 'Este es el producto '),
(161, 31076528, 'Producto numero : 160', 22558, 459, 'Este es el producto '),
(162, 21020657, 'Producto numero : 161', 4043, 858, 'Este es el producto '),
(163, 99067475, 'Producto numero : 162', 16601, 152, 'Este es el producto '),
(164, 72603317, 'Producto numero : 163', 98306, 211, 'Este es el producto '),
(165, 32959450, 'Producto numero : 164', 54147, 263, 'Este es el producto '),
(166, 52640216, 'Producto numero : 165', 31532, 606, 'Este es el producto '),
(167, 49010214, 'Producto numero : 166', 24013, 417, 'Este es el producto '),
(168, 45230440, 'Producto numero : 167', 40541, 538, 'Este es el producto '),
(169, 75740565, 'Producto numero : 168', 74570, 711, 'Este es el producto '),
(170, 38453846, 'Producto numero : 169', 13345, 114, 'Este es el producto '),
(171, 38468807, 'Producto numero : 170', 83451, 706, 'Este es el producto '),
(172, 27760390, 'Producto numero : 171', 29952, 41, 'Este es el producto '),
(173, 75159592, 'Producto numero : 172', 11120, 651, 'Este es el producto '),
(174, 72292683, 'Producto numero : 173', 74726, 441, 'Este es el producto '),
(175, 99279586, 'Producto numero : 174', 69059, 596, 'Este es el producto '),
(176, 87749808, 'Producto numero : 175', 12705, 179, 'Este es el producto '),
(177, 77798762, 'Producto numero : 176', 16485, 621, 'Este es el producto '),
(178, 72162945, 'Producto numero : 177', 38445, 232, 'Este es el producto '),
(179, 43688376, 'Producto numero : 178', 4080, 283, 'Este es el producto '),
(180, 21814167, 'Producto numero : 179', 63756, 336, 'Este es el producto '),
(181, 75011687, 'Producto numero : 180', 83010, 735, 'Este es el producto '),
(182, 17155381, 'Producto numero : 181', 71420, 292, 'Este es el producto '),
(183, 16839752, 'Producto numero : 182', 4911, 210, 'Este es el producto '),
(184, 83866157, 'Producto numero : 183', 15234, 31, 'Este es el producto '),
(185, 34558660, 'Producto numero : 184', 93104, 143, 'Este es el producto '),
(186, 95875402, 'Producto numero : 185', 53873, 522, 'Este es el producto '),
(187, 96859321, 'Producto numero : 186', 42915, 268, 'Este es el producto '),
(188, 88109481, 'Producto numero : 187', 74822, 802, 'Este es el producto '),
(189, 49360525, 'Producto numero : 188', 30953, 312, 'Este es el producto '),
(190, 34224183, 'Producto numero : 189', 52576, 537, 'Este es el producto '),
(191, 89051671, 'Producto numero : 190', 15444, 134, 'Este es el producto '),
(192, 88374999, 'Producto numero : 191', 48809, 419, 'Este es el producto '),
(193, 49713775, 'Producto numero : 192', 28368, 495, 'Este es el producto '),
(194, 16812082, 'Producto numero : 193', 77362, 16, 'Este es el producto '),
(195, 98178950, 'Producto numero : 194', 42373, 62, 'Este es el producto '),
(196, 34097876, 'Producto numero : 195', 71856, 509, 'Este es el producto '),
(197, 16732435, 'Producto numero : 196', 85003, 221, 'Este es el producto '),
(198, 61588442, 'Producto numero : 197', 47188, 550, 'Este es el producto '),
(199, 31651938, 'Producto numero : 198', 15780, 590, 'Este es el producto '),
(200, 18356720, 'Producto numero : 199', 2682, 813, 'Este es el producto '),
(201, 64663195, 'Producto numero : 200', 37065, 948, 'Este es el producto ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idroles` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idroles`, `nombre`) VALUES
(1, 'Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesionesActivas`
--

CREATE TABLE `sesionesActivas` (
  `idsesionesActivas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesionVenta`
--

CREATE TABLE `sesionVenta` (
  `idsesionVenta` int(11) NOT NULL,
  `inicio` datetime DEFAULT NULL,
  `efectivo` int(11) DEFAULT NULL,
  `credito` int(11) DEFAULT NULL,
  `debito` int(11) DEFAULT NULL,
  `vendedor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Vendedores`
--

CREATE TABLE `Vendedores` (
  `idVendedores` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `rol` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Vendedores`
--

INSERT INTO `Vendedores` (`idVendedores`, `nombre`, `cargo`, `rol`) VALUES
(1, 'Mario', 'Administrador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Ventas`
--

CREATE TABLE `Ventas` (
  `idVentas` int(11) NOT NULL,
  `fecha` varchar(45) DEFAULT NULL,
  `vendedor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Ventas`
--

INSERT INTO `Ventas` (`idVentas`, `fecha`, `vendedor`) VALUES
(15, '2019-03-14 15:48:02.457', 1),
(16, '2019-03-14 15:48:10.869', 1),
(17, '2019-03-14 16:07:18.016', 1),
(18, '2019-03-14 16:07:41.074', 1),
(19, '2019-03-14 16:09:00.174', 1),
(20, '2019-03-14 16:09:28.613', 1),
(21, '2019-03-14 16:10:23.786', 1),
(22, '2019-03-14 16:25:50.305', 1),
(23, '2019-03-14 16:26:46.332', 1),
(24, '2019-03-14 16:27:37.132', 1),
(25, '2019-03-14 16:27:41.295', 1),
(26, '2019-03-14 16:28:18.664', 1),
(27, '2019-03-14 16:28:31.125', 1),
(28, '2019-03-14 16:29:24.346', 1),
(29, '2019-03-14 16:30:05.771', 1),
(30, '2019-03-14 16:31:08.813', 1),
(31, '2019-03-14 16:31:48.066', 1),
(32, '2019-03-14 16:35:41.212', 1),
(33, '2019-03-14 16:35:49.956', 1),
(34, '2019-03-14 16:40:59.749', 1),
(35, '2019-03-14 16:41:45.313', 1),
(36, '2019-03-14 16:42:17.661', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalleVenta`
--
ALTER TABLE `detalleVenta`
  ADD PRIMARY KEY (`iddetalleVenta`),
  ADD KEY `fk_venta_idx` (`venta`),
  ADD KEY `fk_producto_idx` (`producto`),
  ADD KEY `fk_vendedor_idx` (`vendedor`);

--
-- Indices de la tabla `Productos`
--
ALTER TABLE `Productos`
  ADD PRIMARY KEY (`idProductos`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idroles`);

--
-- Indices de la tabla `sesionesActivas`
--
ALTER TABLE `sesionesActivas`
  ADD PRIMARY KEY (`idsesionesActivas`);

--
-- Indices de la tabla `sesionVenta`
--
ALTER TABLE `sesionVenta`
  ADD PRIMARY KEY (`idsesionVenta`),
  ADD KEY `fk_vendedor_idx` (`vendedor`);

--
-- Indices de la tabla `Vendedores`
--
ALTER TABLE `Vendedores`
  ADD PRIMARY KEY (`idVendedores`),
  ADD KEY `rol_idx` (`rol`);

--
-- Indices de la tabla `Ventas`
--
ALTER TABLE `Ventas`
  ADD PRIMARY KEY (`idVentas`),
  ADD KEY `vendedor_idx` (`vendedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalleVenta`
--
ALTER TABLE `detalleVenta`
  MODIFY `iddetalleVenta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Productos`
--
ALTER TABLE `Productos`
  MODIFY `idProductos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `idroles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `Vendedores`
--
ALTER TABLE `Vendedores`
  MODIFY `idVendedores` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `Ventas`
--
ALTER TABLE `Ventas`
  MODIFY `idVentas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalleVenta`
--
ALTER TABLE `detalleVenta`
  ADD CONSTRAINT `fk_producto` FOREIGN KEY (`producto`) REFERENCES `Productos` (`idProductos`);

--
-- Filtros para la tabla `sesionesActivas`
--
ALTER TABLE `sesionesActivas`
  ADD CONSTRAINT `fk_sesionesActivas` FOREIGN KEY (`idsesionesActivas`) REFERENCES `sesionVenta` (`idsesionVenta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Vendedores`
--
ALTER TABLE `Vendedores`
  ADD CONSTRAINT `fk_rol` FOREIGN KEY (`rol`) REFERENCES `roles` (`idroles`);

--
-- Filtros para la tabla `Ventas`
--
ALTER TABLE `Ventas`
  ADD CONSTRAINT `fk_vendedor` FOREIGN KEY (`vendedor`) REFERENCES `Vendedores` (`idVendedores`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
